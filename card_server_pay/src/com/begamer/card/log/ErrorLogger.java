package com.begamer.card.log;

import org.apache.log4j.Logger;

/**
 * 错误日志
 * @author LiTao
 * 2014-1-27 上午10:12:12
 */
public class ErrorLogger
{
	public static final Logger logger=Logger.getLogger("ErrorLogger");
	
	public static void print(String s)
	{
		logger.info(s);
	}
}
