/**  
 * @Title: PortalController.java
 * @Package com.bw30.bwr.controller
 * @Description: TODO(用一句话描述该文件做什么)
 * @author 国实  
 * @date Apr 6, 2010 3:34:19 PM
 * @version V1.0  
 */
package com.begamer.card.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;

import com.begamer.card.common.util.PageUtil;
import com.begamer.card.common.util.binRead.BinReader;
import com.begamer.card.common.util.binRead.ServersData;
import com.begamer.card.log.ManagerLogger;
import com.begamer.card.model.dao.UserDao;
import com.begamer.card.model.pojo.PayInfo;

/**
 * 
 * @ClassName: PortalController
 * @author 国实
 * @date Feb 23, 2011 11:12:28 AM
 * 
 */
public class UserController extends AbstractMultiActionController {
	@Autowired
	private UserDao userDao;
	private static Logger managerLogger = ManagerLogger.logger;

	public ModelAndView allPay(HttpServletRequest request,
			HttpServletResponse response) {
		try {
			// 获取当前页数
			int currentpage = Integer.parseInt(request
					.getParameter("pageCurrent"));
			if (currentpage == 0) {
				currentpage = 1;
			}
			// 获取数据的总条数
			int count = userDao.getAll().size();
			// 创建分页实体
			PageUtil page = new PageUtil(count, 20, currentpage);
			List<PayInfo> list = userDao.getAllByPage(page.getCurrentPage(),
					page.getPageSize());
			String href = "user.htm?action=allPay&pageCurrent=";
			request.setAttribute("page", page);
			request.setAttribute("myherf", href);
			request.setAttribute("list", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("/user/pay.vm");
	}

	public ModelAndView serverList(HttpServletRequest request,
			HttpServletResponse response) {
		request.setAttribute("servers", ServersData.getDatas());
		return new ModelAndView("/user/centre.vm");
	}

	public ModelAndView reloadBins(HttpServletRequest request,
			HttpServletResponse response) {
		String reloadKey = request.getParameter("reloadKey");
		if ("9d33o2gm&#LJ@KNNasd./,2".equals(reloadKey)) {
			new BinReader().readAllData();
			request.setAttribute("msg", "重载完毕");
			managerLogger.info("|重载bin文件|登录IP：" + request.getRemoteAddr());
		} else {
			request.setAttribute("msg", "请输入正确的密钥");
		}
		return new ModelAndView("reloadBin.jsp");
	}

}
