<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="com.begamer.card.common.util.binRead.RechargeData"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>模拟充值</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">

	<script type="text/javascript">
		function check()
		{
			var v=document.getElementById("playerId");
			if(v.value==null || v.value=="")
			{
				alert("玩家Id不能为空");
				v.focus();
				return;
			}
			var a = /^(\d*|\-?[1-9]{1}\d*)$/;
			if(!v.value.match(a))
			{
				alert("玩家Id只能是整数");
				v.focus();
				return;
			}
			document.form1.submit();
		}
	</script>
  </head>
  
  <body>
    <form action="gm.htm?action=virtual_pay" method="post" name="form1">
    	<table>
    		<tr align="center"
    		<%
    		if(request.getAttribute("msg")==null)
    			out.println(" style=\"display: none\"");
    		else
    			out.println(" style=\"display: block\"");
    		 %>
    		 >
    			<td colspan="2">
    				<font color="red"><%out.println(request.getAttribute("msg")); %></font>
    			</td>
    		</tr>
    		<tr>
    			<td>密钥</td>
    			<td><input type="text" name="key"/></td>
    		</tr>
    		<tr>
    			<td>玩家Id</td>
    			<td><input type="text" name="playerId" id="playerId"/></td>
    		</tr>
    		<tr>
    			<td>商品Id</td>
    			<td>
    				<select name="rechargeId">
    					<%
    						List<RechargeData> list=RechargeData.getDataList(); 
    						for(RechargeData rd:list)
    						{
    							if(rd.id==1)
    							{
    								out.println("<option value="+rd.id+" selected=\"selected\">"+rd.name+"</option>");
    							}
    							else
    							{
    								out.println("<option value="+rd.id+">"+rd.name+"</option>");
    							}
    						}
    					 %>
    				</select>
    			</td>
    		</tr>
    		<tr>
    			<td colspan="2">
    				<input type="button" value="模拟充值" onclick="javascript:check()"/>
    			</td>
    		</tr>
    	</table>
    </form>
  </body>
</html>
