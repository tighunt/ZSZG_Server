$( function()
	{
		//区服选择事件
		$("#server").change( function()
		{
			if($("#server").val()==""){
				$("#manager").empty();
				$("#model").empty();
			}else{
				$.ajax(
				{
					type :"POST",
					url :"privilege.htm?action=getManagerByServerId",
					data :"serverId=" + $("#server").val(),
					success : function(msg)
					{
						$("#manager").html(msg);
					}
				});
			}
		});
		//GM的双击事件
		$("#manager").dblclick( function()
		{
			$.ajax(
			{
				type :"POST",
				url :"privilege.htm?action=getManagerPrivilege",
				data :"managerId=" + $("#manager").val(),
				success : function(msg)
				{
					$("#model").html(msg);
				}
			});
		});
		
			$("#checkAll").children().click(function(){
			$("[name=model]:checkbox").attr("checked",this.checked);
				});
	});