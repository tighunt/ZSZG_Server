package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class PlayerData implements PropertyReader
{
	public int level;
	public int exp;
	public int maxHp;
	public int maxFight;
	public int maxCard;
	public int maxFriend;
	public int recover;
	
	private static HashMap<Integer, PlayerData> data=new HashMap<Integer, PlayerData>();
	
	@Override
	public void addData()
	{
		data.put(level, this);
	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	public static PlayerData getData(int level)
	{
		return data.get(level);
	}

	@Override
	public void parse(String[] ss)
	{
	}
	
}
