package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.common.util.Random;

public class ShoppvpData implements PropertyReader {
	public int id;
	public int goodstype;
	public int itemId;
	public String name;
	public int costtype;
	public int cost;
	public int number;
	public int vipconfine;
	public int level;
	public int showup;
	public int probability1;
	public int probability2;
	public int probability3;

	private static HashMap<Integer, ShoppvpData> data = new HashMap<Integer, ShoppvpData>();
	private static int total1 = 0;
	private static int total2 = 0;
	private static int total3 = 0;
	@Override
	public void addData() {
		data.put(id, this);
		total1+=probability1;
		total2+=probability2;
		total3+=probability3;
	}

	@Override
	public void parse(String[] ss) {

	}

	@Override
	public void resetData() {
		data.clear();
	}

	/** 根据id获取一个data **/
	public static ShoppvpData getShoppvpData(int index) {
		return data.get(index);
	}
	/**
	 * 商店刷新逻辑
	 * @param type 0、系统和前5次货币刷新1、6-10次货币刷新2、10次以上货币刷新
	 * @return
	 */
	public static String getRandomDatas(int type){
		List<Integer> list = new ArrayList<Integer>();
		String shopItems = "";
		switch (type) {
			case 0:
				int i = 0;
				while(i<10){
					int rand0 = Random.getNumber(total1);
					ShoppvpData shoppvpData = null;
					boolean flag = true;
					for(ShoppvpData sd:data.values()){
						if(null!=sd&&sd.probability1>rand0){
							shoppvpData = sd;
							break;
						}else{
							rand0-=sd.probability1;
						}
					}
					for(int dataId:list){
						if(dataId==shoppvpData.id){
							flag = false;
							break;
						}
					}
					if(flag){
						list.add(shoppvpData.id);
						i++;	
					}
				}
				break;
			case 1:
				int j = 0;
				while(j<10){
					int rand1 = Random.getNumber(total2);
					ShoppvpData shoppvpData = null;
					boolean flag = true;
					for(ShoppvpData sd:data.values()){
						if(null!=sd&&sd.probability2>rand1){
							shoppvpData = sd;
							break;
						}else{
							rand1-=sd.probability2;
						}
					}
					for(int dataId:list){
						if(dataId==shoppvpData.id){
							flag = false;
							break;
						}
					}
					if(flag){
						list.add(shoppvpData.id);
						j++;	
					}
				}
				break;
			case 2:
				int k = 0;
				while(k<10){
					int rand2 = Random.getNumber(total3);
					ShoppvpData shoppvpData = null;
					boolean flag = true;
					for(ShoppvpData sd:data.values()){
						if(null!=sd&&sd.probability3>rand2){
							shoppvpData = sd;
							break;
						}else{
							rand2-=sd.probability3;
						}
					}
					for(int dataId:list){
						if(dataId==shoppvpData.id){
							flag = false;
							break;
						}
					}
					if(flag){
						list.add(shoppvpData.id);
						k++;	
					}
				}
				break;
			default:
				break;
		}
		for(int id:list){
			shopItems+=id+"-"+0+",";
		}
		shopItems = shopItems.substring(0, shopItems.length()-1);
		return shopItems;
	}
}
