package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.cache.PlayerInfo;
import com.begamer.card.common.util.StringUtil;

public class MazeData implements PropertyReader 
{
	/** 迷宫编号* */
	public int id;
	/** 迷宫名称* */
	public String name;
	/** 迷宫icon* */
	public String icon;
	/** 迷宫地图* */
	public String map;
	/** 迷宫场景* */
	public String scene;
	/** 描述* */
	public String description;
	/** 主要产出* */
	public String output;
	/** 解锁条件* */
	public int condition0;
	/** 出现条件* */
	public int condition;
	public int step;//关卡步数
	/** 体力消耗* */
	public int energy;
	/** 免费进入次数* */
	public int freeentry;
	/** 付费重置次数* */
	public int payentry;
	/** 付费花费* */
	public int expense;
	/** 事件及概率 格式：事件-概率* */
	/** 治疗* */
	public List<String> add_pro;//
	/**炸弹**/
	public List<String> cut_pro;
	/** 金币* */
	public List<String> gold_pro;
	/** 道具* */
	public List<String> item_pro;

	private static HashMap<Integer, MazeData> data = new HashMap<Integer, MazeData>();
	public void addData() {
		data.put(id, this);
	}

	@Override
	public void parse(String[] ss) {
		int location =0;
		id =StringUtil.getInt(ss[location]);
		name =StringUtil.getString(ss[location+1]);
		icon =StringUtil.getString(ss[location+2]);
		map =StringUtil.getString(ss[location+3]);
		scene =StringUtil.getString(ss[location+4]);
		description =StringUtil.getString(ss[location+5]);
		output =StringUtil.getString(ss[location+6]);
		condition0 =StringUtil.getInt(ss[location+7]);
		condition =StringUtil.getInt(ss[location+8]);
		step =StringUtil.getInt(ss[location+9]);
		energy =StringUtil.getInt(ss[location+10]);
		freeentry =StringUtil.getInt(ss[location+11]);
		payentry =StringUtil.getInt(ss[location+12]);
		expense =StringUtil.getInt(ss[location+13]);
		
		gold_pro =new ArrayList<String>();
		for(int i=0;i<3;i++)
		{
			location =14+i*2;
			int gold =StringUtil.getInt(ss[location]);
			if(gold ==0)
			{
				continue;
			}
			int pro =StringUtil.getInt(ss[location+1]);
			String goldpro =gold+"-"+pro;
			gold_pro.add(goldpro);
		}
		item_pro =new ArrayList<String>();
		for(int i=0;i<1;i++)
		{
			location =14+3*2+i*2;
			int item =StringUtil.getInt(ss[location]);
			if(item ==0)
			{
				continue;
			}
			int pro =StringUtil.getInt(ss[location+1]);
			
			String itempro =item+"-"+pro;
			item_pro.add(itempro);
		}
		add_pro =new ArrayList<String>();
		for(int i=0;i<1;i++)
		{
			location =14+3*2+1*2+i*2;
			int exp =StringUtil.getInt(ss[location]);
			if(exp ==0)
			{
				continue;
			}
			int pro =StringUtil.getInt(ss[location+1]);
			String exppro =exp+"-"+pro;
			add_pro.add(exppro);
		}
		cut_pro =new ArrayList<String>();
		for(int i=0;i<1;i++)
		{
			location =14+3*2+1*2+1*2+i*2;
			int exp =StringUtil.getInt(ss[location]);
			if(exp ==0)
			{
				continue;
			}
			int pro =StringUtil.getInt(ss[location+1]);
			String exppro =exp+"-"+pro;
			cut_pro.add(exppro);
		}
		addData();
	}

	@Override
	public void resetData() {
		data.clear();
	}

	/** 获取某一个data* */
	public static MazeData getMazeData(int mazeId)
	{
		return data.get(mazeId);
	}

	/** 已经解锁的所有迷宫id* */
	public static List<String> getMazeDatas(int missionId) {
		List<String> list = new ArrayList<String>();
		for (MazeData mData : data.values()) {
			if (mData.condition <= missionId) {
				list.add(mData.id + "");
			}
		}
		return list;
	}

	/** 所有解锁迷宫* */
	public static List<String> getMazes(PlayerInfo pi) {
		List<String> list = new ArrayList<String>();
		String maze =pi.player.getMaze();
		HashMap<Integer, Integer> map =new HashMap<Integer, Integer>();
		if(maze !=null && maze.length()>0)
		{
			String [] temp =maze.split(",");
			for(int k=0;k<temp.length;k++)
			{
				if(temp[k] !=null && temp[k].length()>0)
				{
					String [] ss =temp[k].split("-");
					map.put(StringUtil.getInt(ss[0]),StringUtil.getInt(ss[3]));
				}
			}
		}
		for (MazeData mData : data.values()) {
			if (mData.condition0 == 1)// 等级解锁
			{
				if (mData.condition <= pi.player.getLevel()) {
					MazeCostData mCostData =MazeCostData.getMazeCostData(1);
					if(map.containsKey(mData.id) && map.get(mData.id)>0)
					{
						mCostData =MazeCostData.getMazeCostData(map.get(mData.id)+1);
					}
					list.add(mData.id + "-" + 1 + "-" + mCostData.cost);
				}
			} else if (mData.condition0 == 2)// 关卡解锁
			{
				if (mData.condition <= pi.player.getMissionId()) {
					MazeCostData mCostData =MazeCostData.getMazeCostData(1);
					if(map.containsKey(mData.id) && map.get(mData.id)>0)
					{
						mCostData =MazeCostData.getMazeCostData(map.get(mData.id));
					}
					list.add(mData.id + "-" + 1 + "-" + mCostData.cost);
				}
			}
		}
		return list;
	}

	public static List<String> getMazeDatas(PlayerInfo pi) {
		List<String> list = new ArrayList<String>();
		list = getMazes(pi);
		MazeCostData mCostData =MazeCostData.getMazeCostData(1);
		for (MazeData mData : data.values()) {
			if (mData.condition0 == 1) {
				if (mData.condition > pi.player.getLevel()) {
					list.add(mData.id + "-" + "0" + "-" + mCostData.cost);
				}
			} else if (mData.condition0 == 2) {
				if (mData.condition > pi.player.getMissionId()) {
					list.add(mData.id + "-" + "0" + "-" + mCostData.cost);
				}
			}
		}

		return list;
	}

	/** 获得所有解锁迷宫* */
	public static List<MazeData> getMazeDataj(int missionId) {
		List<MazeData> list = new ArrayList<MazeData>();
		for (MazeData mData : data.values()) {
			if (mData.condition <= missionId) {
				list.add(mData);
			}
		}
		return list;
	}
}
