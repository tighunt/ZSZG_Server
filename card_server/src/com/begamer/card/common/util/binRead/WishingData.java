package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class WishingData implements PropertyReader
{
	public int id;
	public int mazeid;
	public int dropid;
	public int probability;
	public int showprobability;
	
	private static HashMap<Integer, WishingData> data =new HashMap<Integer, WishingData>();
	
	@Override
	public void addData()
	{
		data.put(id, this);
	}

	@Override
	public void parse(String[] ss)
	{

	}

	@Override
	public void resetData()
	{
		data.clear();
	}
	
	public static WishingData getWishingData(int index)
	{
		return data.get(index);
	}

	public static WishingData getWishingDataByDrop(int mazeID,int itemID)
	{
		for(WishingData wd:data.values())
		{
			if(wd.mazeid==mazeID && wd.dropid==itemID)
			{
				return wd;
			}
		}
		return null;
	}
}
