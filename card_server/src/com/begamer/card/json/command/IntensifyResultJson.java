package com.begamer.card.json.command;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.PackElement;

public class IntensifyResultJson extends ErrorJson
{
	/**强化是否成功 默认0成功，1暴击，-1失败**/
	public int state;
	/**强化对象**/
	public PackElement pe;
	
	public int getState()
	{
		return state;
	}
	public void setState(int state)
	{
		this.state = state;
	}
	public PackElement getPe()
	{
		return pe;
	}
	public void setPe(PackElement pe)
	{
		this.pe = pe;
	}
}
