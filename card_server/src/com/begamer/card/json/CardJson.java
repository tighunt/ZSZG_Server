package com.begamer.card.json;

import java.util.List;

public class CardJson {
	public int se;// sequence//
	public int c;// cardid//
	public int lv;// level//
	public int s;// skillId//
	public int slv;// skillLevel//
	public List<String> ps;// passiveSkillId//a
	
	public int b;// ==突破次数,等于5第二天赋有效==//
	public int t1;// 天赋1//
	public int t2;// 天赋2//
	public int t3;// 天赋3//
	
	/** equipId-level **/
	public List<String> es;
	
	/** mission monster使用,固定的atk,def,maxhp **/
	public boolean mm;// missionMonster//
	public int atk;
	public int def;
	public int hp;// maxHp//
	public int cri;
	public int avi;
	
	public int bm;// bossMark//
	
	/**
	 * json插件使用
	 */
	public CardJson()
	{
	}
	
	/**
	 * 玩家使用
	 * 
	 * @param sequence
	 * @param cardId
	 * @param level
	 * @param skillId
	 * @param passiveSkillId
	 * @param equipInfos
	 */
	public CardJson(int sequence, int cardId, int level, int skillId, int skillLevel, List<String> passiveSkillIds, List<String> equipInfos, int breakNum, int talent1, int talent2, int talent3)
	{
		this.mm = false;
		this.se = sequence;
		this.c = cardId;
		this.lv = level;
		this.s = skillId;
		this.slv = skillLevel;
		this.ps = passiveSkillIds;
		this.es = equipInfos;
		this.b = breakNum;
		this.t1 = talent1;
		this.t2 = talent2;
		this.t3 = talent3;
	}
	
	/**
	 * 怪物使用
	 * 
	 * @param sequence
	 * @param cardId
	 * @param level
	 * @param skillId
	 * @param atk
	 * @param def
	 * @param maxHp
	 * @param bossMark
	 */
	public CardJson(int sequence, int cardId, int level, int skillId, int atk, int def, int maxHp, int bossMark, int criRate, int aviRate, int talent1, int talent2, int talent3)
	{
		this.mm = true;
		this.se = sequence;
		this.c = cardId;
		this.lv = level;
		this.s = skillId;
		this.slv = 1;
		this.atk = atk;
		this.def = def;
		this.hp = maxHp;
		this.bm = bossMark;
		this.b = 0;
		this.cri = criRate;
		this.avi = aviRate;
		this.t1 = talent1;
		this.t2 = talent2;
		this.t3 = talent3;
	}
	
	public int getSe()
	{
		return se;
	}
	
	public void setSe(int se)
	{
		this.se = se;
	}
	
	public int getC()
	{
		return c;
	}
	
	public void setC(int c)
	{
		this.c = c;
	}
	
	public int getLv()
	{
		return lv;
	}
	
	public void setLv(int lv)
	{
		this.lv = lv;
	}
	
	public int getS()
	{
		return s;
	}
	
	public void setS(int s)
	{
		this.s = s;
	}
	
	
	public List<String> getPs()
	{
		return ps;
	}

	public void setPs(List<String> ps)
	{
		this.ps = ps;
	}

	public List<String> getEs()
	{
		return es;
	}
	
	public void setEs(List<String> es)
	{
		this.es = es;
	}
	
	public boolean isMm()
	{
		return mm;
	}
	
	public void setMm(boolean mm)
	{
		this.mm = mm;
	}
	
	public int getAtk()
	{
		return atk;
	}
	
	public void setAtk(int atk)
	{
		this.atk = atk;
	}
	
	public int getDef()
	{
		return def;
	}
	
	public void setDef(int def)
	{
		this.def = def;
	}
	
	public int getHp()
	{
		return hp;
	}
	
	public void setHp(int hp)
	{
		this.hp = hp;
	}
	
	public int getBm()
	{
		return bm;
	}
	
	public void setBm(int bm)
	{
		this.bm = bm;
	}
	
	public int getB()
	{
		return b;
	}
	
	public void setB(int b)
	{
		this.b = b;
	}
	
	public int getSlv()
	{
		return slv;
	}
	
	public void setSlv(int slv)
	{
		this.slv = slv;
	}
	
	public int getCri()
	{
		return cri;
	}
	
	public void setCri(int cri)
	{
		this.cri = cri;
	}
	
	public int getAvi()
	{
		return avi;
	}
	
	public void setAvi(int avi)
	{
		this.avi = avi;
	}
	
	public int getT1()
	{
		return t1;
	}
	
	public void setT1(int t1)
	{
		this.t1 = t1;
	}
	
	public int getT2()
	{
		return t2;
	}
	
	public void setT2(int t2)
	{
		this.t2 = t2;
	}
	
	public int getT3()
	{
		return t3;
	}
	
	public void setT3(int t3)
	{
		this.t3 = t3;
	}
	
}