package com.begamer.card.json;

import java.util.List;


public class CGEJson
{
	public List<PackElement> equips;

	public List<PackElement> getEquips()
	{
		return equips;
	}

	public void setEquips(List<PackElement> equips)
	{
		this.equips = equips;
	}

}
