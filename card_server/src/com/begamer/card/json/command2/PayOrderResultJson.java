package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class PayOrderResultJson extends ErrorJson
{
	public String consumValue;
	public String extra;
	public String serverId;
	public String playerId;
	public String userId;
	public int order;

	public int getOrder()
	{
		return order;
	}

	public void setOrder(int order)
	{
		this.order = order;
	}

	public String getConsumValue()
	{
		return consumValue;
	}

	public void setConsumValue(String consumValue)
	{
		this.consumValue = consumValue;
	}

	public String getExtra()
	{
		return extra;
	}

	public void setExtra(String extra)
	{
		this.extra = extra;
	}

	public String getServerId()
	{
		return serverId;
	}

	public void setServerId(String serverId)
	{
		this.serverId = serverId;
	}

	public String getPlayerId()
	{
		return playerId;
	}

	public void setPlayerId(String playerId)
	{
		this.playerId = playerId;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

}
