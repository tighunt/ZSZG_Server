package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.BasicJson;

public class PkBattleLogJson extends BasicJson
{
	public int bNum;//场次
	public List<String> bs;//==合体技Id(0普通技能)-放技能前双方血量(&号分割12个血量)-放技能后双方血量(&号分割12个血量)-释放技能者index-伤害(&号分割若干个伤害)-当前回合数-合体技ids(&分割)==//
	public int r;//1胜利，2失败
	public int getBNum() {
		return bNum;
	}
	public void setBNum(int num) {
		bNum = num;
	}
	public List<String> getBs() {
		return bs;
	}
	public void setBs(List<String> bs) {
		this.bs = bs;
	}
	public int getR() {
		return r;
	}
	public void setR(int r) {
		this.r = r;
	}
}
