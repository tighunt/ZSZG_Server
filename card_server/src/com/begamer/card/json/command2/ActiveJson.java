package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class ActiveJson extends BasicJson {

	private int active;

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}
	
}
