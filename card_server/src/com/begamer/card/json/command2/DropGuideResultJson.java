package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;

public class DropGuideResultJson extends ErrorJson
{
	//==当前itemId==//
	public int id;
	//==关卡id-是否解锁(1解锁)-已完成次数==//
	public List<String> ds;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public List<String> getDs()
	{
		return ds;
	}

	public void setDs(List<String> ds)
	{
		this.ds = ds;
	}
	
}
