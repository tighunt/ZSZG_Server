package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class PowerResultJson extends ErrorJson
{
	public int p;

	public int getP()
	{
		return p;
	}

	public void setP(int p)
	{
		this.p = p;
	}
	
}
