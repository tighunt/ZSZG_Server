package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.element.ShopElement;

public class LevelGiftResultJson extends ErrorJson
{
	public List<ShopElement> s;

	public List<ShopElement> getS() {
		return s;
	}

	public void setS(List<ShopElement> s) {
		this.s = s;
	}
}
