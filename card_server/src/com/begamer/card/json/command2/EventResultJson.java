package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;

public class EventResultJson extends ErrorJson
{
	public List<String> s;//请求关卡选择界面ui25时，格式为关卡id-进入次数(未解锁时候表示解锁等级)-是否解锁(0未解锁，1解锁);请求副本主界面ui24时，格式为副本id-是否开启(0未开启，1开启)-剩余时间
	public int id;
	public int cdtime;//冷却时间--请求某个副本选择关卡时候发送给客户端
	public int num;
	
	public List<String> getS()
	{
		return s;
	}

	public void setS(List<String> s)
	{
		this.s = s;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getCdtime()
	{
		return cdtime;
	}

	public void setCdtime(int cdtime)
	{
		this.cdtime = cdtime;
	}

	public int getNum()
	{
		return num;
	}

	public void setNum(int num)
	{
		this.num = num;
	}
}
