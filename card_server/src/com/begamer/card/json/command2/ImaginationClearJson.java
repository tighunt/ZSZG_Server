package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.BasicJson;

public class ImaginationClearJson extends BasicJson 
{
	/**需清理的物品:类型-物品id-数量.1,垃圾 2,被动技能 3,碎片**/
	public List<String> s;
	/**请求类型：1,一键清理 2,兑换**/
	public int t;

	public List<String> getS() {
		return s;
	}

	public void setS(List<String> s) {
		this.s = s;
	}

	public int getT() {
		return t;
	}

	public void setT(int t) {
		this.t = t;
	}
	
}
