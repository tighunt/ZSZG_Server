package com.begamer.card.model.pojo;

import com.begamer.card.common.util.StringUtil;

public class LogBuy
{
	public int id;
	public int playerId;
	public String buy;
	public String time;
	public int crystal;
	public int crystalPay;
	
	/**
	 * 创建购买日志
	 * lt@2014-9-4 下午03:43:01
	 * @param playerId 玩家Id
	 * @param buy 购买信息
	 * @param crystals 两种钻石消耗数量:普通钻石,充值钻石
	 * @return
	 */
	public static LogBuy createLogBuy(int playerId,String buy,int[] crystals)
	{
		LogBuy lb=new LogBuy();
		lb.playerId=playerId;
		lb.buy=buy;
		lb.time=StringUtil.getDateTime(System.currentTimeMillis());
		lb.crystal=crystals[0];
		lb.crystalPay=crystals[1];
		return lb;
	}
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public int getPlayerId()
	{
		return playerId;
	}
	public void setPlayerId(int playerId)
	{
		this.playerId = playerId;
	}
	public String getBuy()
	{
		return buy;
	}
	public void setBuy(String buy)
	{
		this.buy = buy;
	}
	public String getTime()
	{
		return time;
	}
	public void setTime(String time)
	{
		this.time = time;
	}
	public int getCrystal()
	{
		return crystal;
	}
	public void setCrystal(int crystal)
	{
		this.crystal = crystal;
	}
	public int getCrystalPay()
	{
		return crystalPay;
	}
	public void setCrystalPay(int crystalPay)
	{
		this.crystalPay = crystalPay;
	}
}
