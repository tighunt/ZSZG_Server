package com.begamer.card.model.pojo;

public class Formation 
{
	public Integer id;
	public Integer playerId;
	/**index-index-index-index-index-index**/
	public String cardInfo;
	/**index-index-index-index-index-index**/
	public String skillInfo;
	/**
	 * index&index&index-index&index&index-index&index&index-index&index&index-index&index&index-index&index&index
	 * 注：如果被动技能为空的话就用字符x代替，如：index&x&index-x&x&index-x&x&x
	 */
	public String passiveSkillInfo;
	/**index&index&index-index&index&index-index&index&index-index&index&index-index&index&index-index&index&index**/
	public String equipInfo;
	/**index-index**/
	public int unitSkillInfo;
	
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public Integer getPlayerId()
	{
		return playerId;
	}
	public void setPlayerId(Integer playerId)
	{
		this.playerId = playerId;
	}
	public String getCardInfo()
	{
		return cardInfo;
	}
	public void setCardInfo(String cardInfo)
	{
		this.cardInfo = cardInfo;
	}
	public String getSkillInfo()
	{
		return skillInfo;
	}
	public void setSkillInfo(String skillInfo)
	{
		this.skillInfo = skillInfo;
	}
	public String getPassiveSkillInfo()
	{
		return passiveSkillInfo;
	}
	public void setPassiveSkillInfo(String passiveSkillInfo)
	{
		this.passiveSkillInfo = passiveSkillInfo;
	}
	public String getEquipInfo()
	{
		return equipInfo;
	}
	public void setEquipInfo(String equipInfo)
	{
		this.equipInfo = equipInfo;
	}
	public int getUnitSkillInfo()
	{
		return unitSkillInfo;
	}
	public void setUnitSkillInfo(int unitSkillInfo)
	{
		this.unitSkillInfo = unitSkillInfo;
	}
}
