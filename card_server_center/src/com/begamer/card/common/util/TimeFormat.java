package com.begamer.card.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * 
 * @ClassName: TimeFormat
 * @Description: TODO 时间格式转换
 * @author gs
 * @date Nov 10, 2011 3:54:00 PM
 * 
 */
public class TimeFormat {

	/**
	 * 
	 * @Title: getFormatStringByDate
	 * @Description: TODO 返回String型时间 yyyy-MM-dd hh:mm:ss
	 */
	public static String getFormatStringByDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("",
				Locale.SIMPLIFIED_CHINESE);
		sdf.applyPattern("yyyy-MM-dd hh:mm:ss");
		String timeString = sdf.format(date);
		return timeString;
	}

	public static String getTimeStringByDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("",
				Locale.SIMPLIFIED_CHINESE);
		sdf.applyPattern("HHmmssSSS");
		String timeString = sdf.format(date);
		return timeString;
	}

	public static void main(String[] args) {
		System.out
				.println("time:" + TimeFormat.getTimeStringByDate(new Date()));
	}
}
