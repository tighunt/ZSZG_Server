package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class ChannelData implements PropertyReader
{
	public String id;
	public String name;
	public String packagePath;
	
	private static HashMap<String, ChannelData> data =new HashMap<String, ChannelData>();
	@Override
	public void addData() {
		data.put(id, this);
	}

	@Override
	public void parse(String[] ss) {

	}

	@Override
	public void resetData() {
		data.clear();
	}

	public static ChannelData getChannelData(String str)
	{
		return data.get(str);
	}
}
