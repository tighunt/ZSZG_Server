package com.begamer.card.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import com.begamer.card.common.util.binRead.ServersData;

public class CentreController extends AbstractMultiActionController {

	public ModelAndView centre(HttpServletRequest request,
			HttpServletResponse response) {
		request.setAttribute("servers", ServersData.getDatas());
		return new ModelAndView("/centre/centre.vm");
	}
}
